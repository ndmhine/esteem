import setuptools

install_requires = [
    'ase>=3.19.1',
    'numpy>=1.11.3',
    'scipy>=0.18.1',
    'matplotlib>=2.0.0',
]

#with open("README.md", "r") as fh:
#    long_description = fh.read()
long_description = ""

from nbconvert import PythonExporter;
from nbconvert.writers import FilesWriter;
from os import makedirs, path

def convert_ipynb(nb,dir):
   py_exp = PythonExporter()
   py_write = FilesWriter()
   py = nb.lower()
   try:
       (body,resources) = py_exp.from_file(nb+".ipynb");
       py_write.write(output=body,resources=resources,notebook_name=f"{dir}/{py}")
   except:
       print("Failed when converting ",nb," in directory ",dir)

def build_py():
   # Convert notebooks to python
   task_nbs = ["QMD_Trajectories", "ML_Training", "Clusters", "Solvate", "Solutes",
               "Spectra", "ML_Testing", "ML_Trajectories", "ML_Debugger"]
   wrapper_nbs = ["Amber","ONETEP","NWChem","ORCA","LAMMPS","AMP","PhysNet","MACE","ezFCF","SpecPyCode"]
   base_nbs = ["Drivers","Parallel","Trajectories","Active_Learning"]
   base = "esteem"
   subdir = ["wrappers","tasks"]
   if not path.exists(base):
      print(f'Creating directory: {base}')
      makedirs(base)
   open(base+'/__init__.py', 'a').close()
   for s in subdir:
      if not path.exists(base+'/'+s):
         print(f'Creating directory: {base}/{s}')
         makedirs(base+"/"+s)
      open(base+'/'+s+'/__init__.py', 'a').close()
   for nb in base_nbs:
       convert_ipynb(nb,base)
   for nb in task_nbs:
      convert_ipynb(nb,base+"/tasks")
   for nb in wrapper_nbs:
      convert_ipynb(nb,base+"/wrappers")

build_py()

if False: # not yet ready for proper installs
    setuptools.setup(
    name="esteem-ndmhine", # Replace with your own username
    version="1.0.0b",
    author="Nicholas Hine",
    author_email="n.d.m.hine@warwick.ac.uk",
    description="Explicit Solvent Toolkit for Electronic Excitations of Molecules",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.com/ndmhine/esteem",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: TBD",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    )   

